package com.citizenweb.training.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.citizenweb.training.classes.AbstractMyClass;
import com.citizenweb.training.classes.MyClassA;
import com.citizenweb.training.classes.MyClassB;
import com.citizenweb.training.interfaces.IClassFactory;

@Service
public class ClassFactory implements IClassFactory {
	
	@Autowired
	private MyClassA classA;
	@Autowired
	private MyClassB classB;
	
	@Override
	public AbstractMyClass getA(String name) {
		return classA.setName(name);
	}

	@Override
	public AbstractMyClass getB(String name) {
		return classB.setName(name);
	}

}
