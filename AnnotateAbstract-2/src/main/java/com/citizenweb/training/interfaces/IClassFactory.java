package com.citizenweb.training.interfaces;

import com.citizenweb.training.classes.AbstractMyClass;

public interface IClassFactory {

	public AbstractMyClass getA(String name);
	
	public AbstractMyClass getB(String name);
	
}
