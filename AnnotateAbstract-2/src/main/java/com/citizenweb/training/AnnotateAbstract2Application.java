package com.citizenweb.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnnotateAbstract2Application {

	public static void main(String[] args) {
		SpringApplication.run(AnnotateAbstract2Application.class, args);
	}

}
