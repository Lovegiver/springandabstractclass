package com.citizenweb.training.classes;

import org.springframework.beans.factory.annotation.Autowired;

import com.citizenweb.training.interfaces.IMyService;

public abstract class AbstractMyClass {
	
	@Autowired
	private IMyService serv;
	
	private String name;
	
	protected AbstractMyClass() {}
	
	public AbstractMyClass(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public AbstractMyClass setName(String name) {
		this.name = name;
		return this;
	}
	
	public abstract void doAction();
	
	public void doElsething(String whichClass) {
		System.out.println("My Abstract class action lauched by " + whichClass);
		serv.doSomething();
		System.out.println("End of Abstract action");
	}
	
}
