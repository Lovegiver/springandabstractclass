package com.citizenweb.training.classes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citizenweb.training.interfaces.IMyService;

@Component
public class MyClassA extends AbstractMyClass {
	
	@Autowired
	private IMyService serv;

	private String myClassName;
	
	@SuppressWarnings("unused")
	private MyClassA() {}
	
	public MyClassA(String name) {
		super(name);
	}

	public String getMyClassName() {
		return myClassName;
	}

	public MyClassA setMyClassName(String myClassName) {
		this.myClassName = myClassName;
		return this;
	}

	@Override
	public void doAction() {
		System.out.println("Action de MyClass A");
		System.out.printf("My NAME is %s, my CLASSNAME is %s\n", super.getName(), this.myClassName);
		serv.doSomething();
		System.out.println("Fin Action de MyClass A");
	}

}
