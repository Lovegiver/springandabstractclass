package com.citizenweb.training.classes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citizenweb.training.interfaces.IMyService;

@Component
public class MyClassB extends AbstractMyClass {

	@Autowired
	private IMyService serv;

	private String myClassName;
	private String myClassCity;
	
	@SuppressWarnings("unused")
	private MyClassB() {}
	
	public MyClassB(String name, String city) {
		super(name);
		this.myClassCity = city;
	}

	public String getMyClassName() {
		return myClassName;
	}

	public MyClassB setMyClassName(String myClassName) {
		this.myClassName = myClassName;
		return this;
	}
	
	public String getMyClassCity() {
		return myClassCity;
	}

	public MyClassB setMyClassCity(String myClassCity) {
		this.myClassCity = myClassCity;
		return this;
	}

	@Override
	public void doAction() {
		System.out.println("Action de MyClass B");
		System.out.printf("My NAME is %s, my CLASSNAME is %s, my CLASSCITY is %s\n", super.getName(), this.myClassName, this.myClassCity);
		serv.doSomething();
		System.out.println("Fin Action de MyClass B");
	}

}
