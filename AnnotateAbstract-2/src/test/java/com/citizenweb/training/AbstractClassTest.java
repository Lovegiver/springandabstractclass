package com.citizenweb.training;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.citizenweb.training.classes.AbstractMyClass;
import com.citizenweb.training.interfaces.IClassFactory;


@SpringBootTest
class AbstractClassTest {
	
	@Autowired
	private IClassFactory factory;

	@Test
	void testGetName() {
		AbstractMyClass myA = factory.getA("MY_TEST_A");
		assertTrue(myA.getName().equals("MY_TEST_A"));
	}

	@Test
	void testSetName() {
		AbstractMyClass myB = factory.getB("MY_TEST_B");
		assertTrue(myB.getName().equals("MY_TEST_B"));
		myB.setName("CHANGED_TEST_B");
		assertTrue(myB.getName().equals("CHANGED_TEST_B"));
	}

	@Test
	void testDoAction() {
		AbstractMyClass myB = factory.getB("MY_ACTION_B");
		assertTrue(myB.getName().equals("MY_ACTION_B"));
		myB.doAction();
	}

	@Test
	void testDoElsething() {
		AbstractMyClass myA = factory.getA("MY_ACTION_ABSTRACT");
		assertTrue(myA.getName().equals("MY_ACTION_ABSTRACT"));
		myA.doElsething("WORKING_FROM_ABSTRACT");
	}
	

}
