package com.citizenweb.training;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.citizenweb.training.classes.AbstractMyClass;
import com.citizenweb.training.classes.MyClassA;
import com.citizenweb.training.classes.MyClassB;
import com.citizenweb.training.interfaces.IClassFactory;

@SpringBootTest
class AnnotationTest {

	@Autowired
	private IClassFactory facto;
	
	@Test
	void isServiceInstantiatedTest() {

		AbstractMyClass myA = facto.getA("nameA");
		((MyClassA) myA).setMyClassName("myClassNameA");
		myA.doAction();
		
		AbstractMyClass myB = facto.getB("nemB");
		((MyClassB) myB).setMyClassName("myClassNameB").setMyClassCity("Paris");
		myB.doAction();
		
		myA.doElsething("A");
		
		myB.doElsething("B");
		
	}

}
